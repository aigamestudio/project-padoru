﻿using UnityEngine;

public class PadoruBehaviour : MonoBehaviour {
	int padoruCount = 0;

	AudioSource audioSource;

	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
	}
	
	public void PlayOneShot(AudioClip clip) {
		padoruCount++;
		audioSource.PlayOneShot(clip);
	}
}
